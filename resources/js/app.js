require('./bootstrap');
import Vue from 'vue';
import Ecomerce from './components/App'
import AllStore from './store/AllStore';
import router from './router'
Vue.router = router;

Vue.config.productionTip = false;


const app = new Vue({
    el: '#app',
    router,
    store: AllStore,
    components: {
        Ecomerce
    }
});
