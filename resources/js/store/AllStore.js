import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import {RESOURCE_GETUSERS, RESOURCE_GETPOSTS, RESOURCE_GETIMAGES, RESOURCE_GETCATEGORIES} from '../api';

Vue.use(Vuex);

const user = {
    namespaced: true,
    state: {
        users: [],
        user: {},
    },
    mutations: {
        FETCH(state, users) {
            state.users = users;
        },
        FETCH_ONE(state, user) {
            state.user = user;
        },
    },
    actions: {
        fetch({ commit }) {
            return axios.get(RESOURCE_GETUSERS)
                .then(response => commit('FETCH', response.data))
                .catch();
        },
        fetchOne({ commit }, id) {
            axios.get(`${RESOURCE_USER}/${id}/edit`)
                .then(response => commit('FETCH_ONE', response.data))
                .catch();
        },
    }
};

const post = {
    namespaced: true,
    state: {
        posts: [],
    },
    mutations: {
        FETCH(state, posts) {
            state.posts = posts;
        },
    },
    actions: {
        fetch({ commit }) {
            return axios.get(RESOURCE_GETPOSTS)
                .then(response => commit('FETCH', response.data))
                .catch();
        },
    }
};

const image = {
    namespaced: true,
    state: {
        images: [],
    },
    mutations: {
        FETCH(state, images) {
            state.images = images;
        },
    },
    actions: {
        fetch({ commit }) {
            return axios.get(RESOURCE_GETIMAGES)
                .then(response => commit('FETCH', response.data))
                .catch();
        },
    }
};

const categories = {
    namespaced: true,
    state: {
        categories: [],
    },
    mutations: {
        FETCH(state, categories) {
            state.categories = categories;
        },
    },
    actions: {
        fetch({ commit }) {
            return axios.get(RESOURCE_GETCATEGORIES)
                .then(response => commit('FETCH', response.data))
                .catch();
        },
    }
};

const allStore = new Vuex.Store({
    modules: {
        user,
        post,
        image,
        categories,
    }
});

export default allStore;
