import Vue from 'vue'
import VueRouter from 'vue-router'

import Template from "../components/Template";
Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'template',
            component: Template
        }
    ]
});
export default router;
