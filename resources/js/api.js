const RESOURCE_GETUSERS = 'users/getUsers';
const RESOURCE_GETPOSTS = 'posts/getPosts';
const RESOURCE_GETIMAGES = 'uploads/images';
const RESOURCE_GETCATEGORIES = 'categories/getCategories';

export {
    RESOURCE_GETUSERS,
    RESOURCE_GETPOSTS,
    RESOURCE_GETIMAGES,
    RESOURCE_GETCATEGORIES
};
