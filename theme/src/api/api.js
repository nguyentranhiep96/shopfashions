const BASE_URL_API = 'http://dev.shopfashion.test';

// URI API
const API_LOGIN = BASE_URL_API + '/api/login/';
const API_LOGOUT = BASE_URL_API + '/api/logout/';

export default {
    BASE_URL_API,
    API_LOGIN,
    API_LOGOUT
}
