import api from './api'
import axios from 'axios';

// config header
const config = {
    headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        "Authorization" : `Bearer ${JSON.parse(localStorage.getItem('access_token'))}`
    },
}

// base axios
export default {
    getAxios(url) {
        return axios.get(api.BASE_URL_API + '/' + url, config)
    },
    getAxiosBy(url, key) {
        return axios.get(api.BASE_URL_API + '/' + url, key, config)
    },
    postAxios(url, data = null) {
        return axios.post(api.BASE_URL_API + '/' + url, data, config)
    },
    putAxios(url, data) {
        return axios.put(api.BASE_URL_API + '/' + url, data, config)
    },
    patchAxios(url, data) {
        return axios.patch(api.BASE_URL_API + '/' + url, data, config)
    },
    deleteAxios(url) {
        return axios.delete(api.BASE_URL_API + '/' + url, config)
    },
}
