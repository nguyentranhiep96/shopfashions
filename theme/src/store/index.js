import Vue from 'vue'
import Vuex from 'vuex'
import role from './modules/roles'
import user from './modules/users'
import auth from './modules/auth'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
      role,
      user,
      auth,
  }
})
