import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import callApi from '../../api/index'
Vue.use(VueAxios, axios);

const namespaced = true;
export default {
    namespaced,
    state: {
        roles: [],
        role: {},
        pagination: {},
    },
    mutations: {
        getRole(state, role) {
            state.roles = role;
        },
        setPagination(state, page){
            state.pagination = page;
        },
    },
    actions: {
        getListRole({commit}, page = 1) {
            return new Promise((resolve, reject) => {
                callApi.getAxios('api/roles?page=' + page).then(response => {
                    resolve(response.data)
                    commit('getRole', response.data.data.data)
                    commit('setPagination', response.data.data)
                }).catch(error => {
                    reject("error" +error)
                })
            })
        },
        addRole(context, role) {
            return new Promise((resolve, reject) => {
                callApi.postAxios('api/roles', role).then(response => {
                    resolve(response)
                }).catch(error => {
                    reject("error" + error)
                })
            })
        },
        delete(context, ids) {
            return new Promise((resolve, reject) => {
                callApi.deleteAxios('api/roles/' + ids).then(response => {
                    resolve(response.data)
                }).catch(error => {
                    reject("Error" +error)
                })
            })
        },
        edit(context, id) {
            return new Promise((resolve, reject) => {
                callApi.getAxios('api/roles/' + id + '/edit').then(response => {
                    resolve(response.data)
                    console.log(response.data)
                }).catch(error => {
                    reject("Error" +error)
                })
            })
        }
    }
}
