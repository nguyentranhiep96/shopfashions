import Vue from 'vue';
import apiResource from '../../api/api'
import callApi from '../../api/index'
import axios from 'axios';
import VueAxios from 'vue-axios';
Vue.use(VueAxios, axios);

const namespaced = true;
export default {
    namespaced,
    state: {
        isLoggedIn: false,
        access_token: null
    },
    mutations: {
        setLogin(state, access_token) {
            state.loggedIn = true
            state.access_token = access_token
        },
        setLogout: (state) => {
            state.loggedIn = false;
        },
    },
    actions: {
        login(context, user) {
            return new Promise((resolve, reject) => {
                axios.post(apiResource.BASE_URL_API +'/api/login', {
                    email: user.email,
                    password: user.password,
                }).then(response => {
                    if (response.data.access_token) {
                        localStorage.setItem('access_token', JSON.stringify(response.data.access_token));
                        context.commit('setLogin', response.data.access_token)
                    }
                    resolve("oke")
                }).catch(error => {
                    reject("Error"+error)
                })
            })
        },
        logout({commit}) {
            callApi.postAxios('api/logout').then(() => {
                localStorage.removeItem('access_token');
                commit('setLogout')
            })
        }
    }
}
