import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store/index'
import TheTemplate from '../containers/Template'
import TheDashboard from '../views/Dashboard/Dashboard'
import Users from '../views/Users/Users'
import Roles from '../views/Roles/Roles'
import Products from '../views/Products/Products'


Vue.use(VueRouter)

const routes = [
    {
        path: '/login',
        name: 'Login',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "PageLogin" */ '../pages/Login.vue')
    },
    {
        path: '/',
        redirect: '/dashboard',
        name: 'Template',
        component: TheTemplate,
        children: [
            {
                path: '/dashboard',
                name: 'Dashboard',
                component:TheDashboard
            },
            {
                path: '/users',
                name: 'User',
                component:Users
            },
            {
                path: '/roles',
                name: 'Role',
                component:Roles
            },
            {
                path: '/products',
                name: 'Product',
                component:Products
            }
        ]
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

router.beforeEach((to, from, next) => {
    const publicPages = ['/login'];
    const authRequired = !publicPages.includes(to.path);
    const accessTokenServer = store.state.auth.access_token;
    const access_token = localStorage.getItem('access_token');

    if (authRequired && !access_token && !accessTokenServer) {
        return next('/login');
    }
    if(to.path==='/login' && access_token && accessTokenServer) {
        return next('/');
    }
    next();
});

export default router
