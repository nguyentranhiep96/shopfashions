<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::namespace('Api')->group(function () {
    Route::post('login', 'Auth\AuthController@login');
    Route::middleware('auth:api')->group(function () {
        Route::namespace('Auth')->group(function () {
            Route::post('logout', 'AuthController@logout');
            Route::post('refresh', 'AuthController@refresh');
            Route::get('me', 'AuthController@me');
            Route::get('users/profile', 'AuthController@profile');
        });

        Route::namespace('Roles')->group(function () {
            Route::get('/roles', 'RoleController@index');
            Route::post('roles', 'RoleController@store');
            Route::delete('roles/{role}', 'RoleController@destroy');
            Route::get('roles/{role}/edit', 'RoleController@edit');
        });
    });
});

