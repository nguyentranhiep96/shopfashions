<?php

namespace App\Repositories\modules\category;

use App\Models\Category;
use App\Repositories\base\BaseRepository;
use App\Repositories\modules\category\CategoryRepositoryInterface;
use Illuminate\Support\Str;

class CategoryRepository extends BaseRepository implements CategoryRepositoryInterface
{
    public function model()
    {
        return Category::class;
    }

    public function save(array $data, int $id = null)
    {
        return parent::save([
            'category_name' => $data['category_name'],
            'category_slug' => Str::slug($data['category_name'], '-'),
            'category_description' => $data['category_description'],
            'category_status' => $data['category_status'],
            'created_by' => auth()->user()->name,
        ], $id);
    }

    public function searchCategory($data)
    {
        return parent::query()->where('category_name', 'like', '%' . $data . '%')->orderBy('id', 'DESC')->paginate(10);
    }
}
