<?php

namespace App\Repositories\modules\category;

interface CategoryRepositoryInterface
{
    public function save(array $data, int $id = null);

    public function searchCategory($data);
}
