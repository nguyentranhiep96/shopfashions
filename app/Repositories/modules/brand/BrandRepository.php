<?php

namespace App\Repositories\modules\brand;

use App\Models\Brand;
use App\Repositories\base\BaseRepository;
use App\Repositories\modules\brand\BrandRepositoryInterface;
use Illuminate\Support\Str;

class BrandRepository extends BaseRepository implements BrandRepositoryInterface
{
    public function model()
    {
        return Brand::class;
    }

    public function save(array $data, int $id = null)
    {
        return parent::save([
            'brand_name' => $data['brand_name'],
            'brand_slug' => Str::slug($data['brand_name'], '-'),
            'brand_description' => $data['brand_description'],
            'brand_status' => $data['brand_status'],
            'created_by' => auth()->user()->name,
        ], $id);
    }

    public function searchBrand($data)
    {
        return parent::query()->where('brand_name', 'like', '%' . $data . '%')->orderBy('id', 'DESC')->paginate(10);
    }
}
