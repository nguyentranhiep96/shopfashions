<?php

namespace App\Repositories\modules\brand;

interface BrandRepositoryInterface
{
    public function save(array $data, int $id = null);

    public function searchBrand($data);
}
