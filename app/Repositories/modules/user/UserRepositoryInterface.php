<?php

namespace App\Repositories\modules\user;

interface UserRepositoryInterface
{
    public function save(array $data, int $id = null);

    public function searchUser($data);

    public function updateProfile(array $data, int $id = null);

    public function changePassword(array $data, int $id = null);
}
