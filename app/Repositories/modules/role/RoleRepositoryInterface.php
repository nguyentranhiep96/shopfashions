<?php

namespace App\Repositories\modules\role;

interface RoleRepositoryInterface
{
    public function findByRole();

    public function save(array $data, int $id = null);

    public function searchRole($data);
}
