<?php

namespace App\Repositories\modules\product;

use App\Models\Product;
use App\Repositories\base\BaseRepository;
use App\Repositories\modules\product\ProductRepositoryInterface;

class ProductRepository extends BaseRepository implements ProductRepositoryInterface
{
    public function model()
    {
        return Product::class;
    }

    public function getRelationBy($relation) {
        return $this->model->with($relation);
    }
}
