<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\modules\user\UserRepository;
use App\Services\AuthService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * @SWG\Swagger(
     *      schemes={"http", "https"},
     *      @SWG\Info(
     *          version="1.0.0",
     *          title="Shop Fashion ",
     *          description="Shop Fashion  API description",
     *          @SWG\Contact(
     *              email="darius@matulionis.lt"
     *          ),
     *      )
     *  )
     */
    /**
     * @SWG\SecurityScheme(
     *   securityDefinition="APIKeyHeader",
     *   type="apiKey",
     *   in="header",
     *   name="Authentication",
     * )
     */

    /**
     * @SWG\Post(
     *   path="/api/login",
     *   summary="Login Form",
     *   operationId="login",
     *   tags={"Auth"},
     *   security={
     *       {"ApiKeyAuth": {}}
     *   },
     *   @SWG\Parameter(
     *       name="email",
     *       in="formData",
     *       required=true,
     *       type="string"
     *   ),
     *   @SWG\Parameter(
     *       name="password",
     *       in="formData",
     *       required=true,
     *       type="string"
     *   ),
     *   @SWG\Response(response=200, description="login successful "),
     *   @SWG\Response(response=401, description="Unauthozire"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    private $authService;
    private $userRepository;

    public function __construct(AuthService $authService, UserRepository $userRepository)
    {
        $this->authService = $authService;
        $this->userRepository = $userRepository;
    }

    public function guard()
    {
        return Auth::guard();
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]);
    }

    public function me()
    {
        return response()->json($this->guard()->user());
    }

    public function logout()
    {
        $this->guard()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    public function login(Request $request)
    {
        try {
            $credentials = $request->only('email', 'password');

            if ($token = $this->guard()->attempt($credentials)) {
                return $this->respondWithToken($token);
            } else {
                return response()->json([
                    'error' => 'Unauthorised'
                ], 401);
            }
        } catch (\Exception $e) {
            return response()->json([
                'code' => 500,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function profile()
    {
        try {
            $user = auth()->user();
            return response()->json([
                'message' => 'success',
                'data' => $user
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'code' => 500,
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
