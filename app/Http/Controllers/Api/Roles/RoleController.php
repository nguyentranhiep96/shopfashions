<?php

namespace App\Http\Controllers\api\Roles;

use App\Http\Controllers\Controller;
use App\Http\Requests\roles\CreateRoleRequest;
use App\Http\Requests\roles\UpdateRoleRequest;
use App\Repositories\modules\permission\PermissionRepository;
use App\Repositories\modules\role\RoleRepository;
use App\Services\StatusResponse;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    use StatusResponse;
    protected $roleRepository;

    protected $permissionRepository;

    public function __construct(RoleRepository $roleRepository, PermissionRepository $permissionRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
    }

    public function index()
    {
        $roles = $this->roleRepository->paginateBy('*', 'role_level', 1, 5, 'DESC');
        return $this->responseStatus(200, 'Success', $roles);
    }

    public function store(Request $request)
    {
        try {
            $roles = $this->roleRepository->save($request->all());
            return $this->responseStatus(201, 'Create Success', $roles);
        } catch (\Exception $e) {
            return $this->responseException(500, $e);
        }
    }

    public function show($id)
    {
        try {
            $role = $this->roleRepository->findById($id);
            return $this->responseStatus(200, 'Success', $role);
        } catch (\Exception $e) {
            return $this->responseException(500, $e);
        }
    }

    public function edit($id)
    {
        try {
            $role = $this->roleRepository->findById($id);
            return $this->responseStatus(200, 'Success', $role);
        } catch (\Exception $e) {
            return $this->responseException(500, $e);
        }
    }

    public function update(UpdateRoleRequest $request, $id)
    {
        try {
            $role = $this->roleRepository->save($request->all(), $id);
            return $this->responseStatus(200, 'Update Role Success', $role);
        } catch (\Exception $e) {
            return $this->responseException(500, $e);
        }
    }

    public function destroy($id)
    {
        try {
            $ids = explode(",", $id);
            $this->roleRepository->delete($ids);
            return $this->responseStatus(200, 'Delete Role Success');
        } catch (\Exception $e) {
            return $this->responseException(500, $e);
        }
    }

    public function search($query)
    {
        $roles = $this->roleRepository->searchRole($query);
        return view('admins.modules.roles.table', compact('roles'));
    }

    public function showPermissionsByRole($role_id)
    {
        try {
            $permissions = $this->permissionRepository->findBy('permission_level', 1)->get();
            $role = $this->roleRepository->findById($role_id);
            $permissionsOfRole = $role->permissions()->get();
            $data = [];
            foreach ($permissionsOfRole as $item) {
                $permissionsOfRoleById = $item->id;
                array_push($data, $permissionsOfRoleById);
            }
            return view('admins.modules.roles.permission', compact('permissions', 'data'));
        } catch (\Exception $e) {
            return $this->responseException(500, $e);
        }
    }

    public function setPermissionsForRole(Request $request, $role_id, $permission_id)
    {
        try {
            $ids = explode(",", $permission_id);
            $role = $this->roleRepository->findById($role_id);
            $role->permissions()->sync($ids);
            return $this->responseStatus(200, 'Success');
        } catch (\Exception $e) {
            return $this->responseException(500, $e);
        }
    }
}
