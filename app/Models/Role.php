<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public $timestamps = true;
    protected $table = 'roles';
    protected $fillable = [
        'role_name',
        'role_label',
        'role_description',
        'role_status',
        'created_by'
    ];

    public function permissions()
    {
        return $this->belongsToMany('App\Models\Permission', 'permission_role', 'role_id', 'permission_id');
    }

    public function users()
    {
        return $this->hasMany('App\Models\User', 'role_id');
    }
}
